import sinon, { SinonSandbox, SinonStub } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { PurgeWorker } from "#ie/PurgeWorker";

describe("PurgeWorker", () => {
    let worker: PurgeWorker;
    let sandbox: SinonSandbox;
    let queryStub: SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        queryStub = sandbox.stub().resolves("fake delete");
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ query: queryStub }));

        worker = new PurgeWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by deleteOldVehiclePositions method", async () => {
        await worker.deleteOldVehiclePositions({});
        sandbox.assert.calledThrice(queryStub);
    });
});
